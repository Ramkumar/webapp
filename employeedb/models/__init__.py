'''
 Models for this project implemented with MongoKit.
'''
from .dbconfig import db
from .employeedb import Employee
from .user import UserLog

db.register([ Employee, UserLog,])


