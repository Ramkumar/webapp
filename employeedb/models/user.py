from datetime import datetime
from flask.ext.mongokit import MongoKit, Document
from flaskext.login import UserMixin
from .dbconfig import db




class UserLog(Document, UserMixin):
    use_dot_notation = True
    skip_validation = True
    __collection__ = 'user'
    structure = {
        'username': unicode,
        'password' : unicode,
        'is_active' : unicode,
        }
    required_fields = ['username', 'password']
                        
    default_values = {'username': 'admin','password':'admin123'}

    @staticmethod
    def get_by(name):
        """ This is used by flask-login to get the user object given email """ 
        return db.User.find_one({"username": (name)})
    
    def get_id(self):
        return str(self._id)
    
    @staticmethod
    def get_all():
        users = db.User.find()
        return list(users)

    def is_active():
        return True
