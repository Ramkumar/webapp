from datetime import datetime
from flask.ext.mongokit import MongoKit, Document
from pymongo.objectid import ObjectId
from .dbconfig import db


class Employee(Document):
    use_dot_notation = True
    skip_validation = True
    __collection__ = 'emps'
    structure = {
        '_id':unicode,
        'empname': unicode,   
        'fathername': unicode,        #Father name  
        'mothername': unicode,        #Mother  name  
        'qualification': unicode, 
        'nationality': unicode,   
        'dob': unicode,           #Date of birth
        'doj': unicode,           #Date of Joining
        'gender': [unicode],
        'mstatus': unicode,      #Marital Status
        'sname': unicode,        #Spouse Name
        'designation': unicode,
        'department': unicode,
        'email': unicode,
        'pan': unicode,
        'mobile': unicode,
        'creation': datetime,  # Employee  creation date
        
    }
    required_fields = ['_id',
                       'empname',
                       'fathername',
                       'mothername',
                       'qualification',
                       'nationality',
                       'dob',
                       'doj',
                       'gender',
                       'mstatus',
                       'sname',
                       'designation',
                       'department',
                       'email',
                       'pan',
                       'mobile'
                    ]
    default_values = {'creation': datetime.utcnow()}
    
    @staticmethod
    def delete(employee_id):
        db.emps.remove({'_id': employee_id})
    
    @staticmethod
    def get_by_id(employee_id):
        return db.Employee.find_one({'_id': employee_id})


    @staticmethod
    def update(f, employee):
        emp = db.Employee.get_by_id(employee)
        emp._id = f.empid.data
        emp.empname = f.empname.data
        emp.fathername = f.fathername.data
        emp.mothername = f.mothername.data
        emp.qualification = f.qualification.data
        emp.nationality = f.nationality.data
        emp.dob = f.dob.data
        emp.doj = f.doj.data
        emp.gender = f.gender.data
        emp.mstatus = f.mstatus.data
        emp.sname = f.sname.data
        emp.designation = f.designation.data
        emp.department = f.department.data
        emp.email = f.email.data
        emp.pan = f.pan.data
        emp.mobile = f.mobile.data
        emp.save()
        return emp



