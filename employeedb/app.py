'''
 Make the flask application here.
'''

from flask import (Flask, g, render_template, request, redirect, abort, url_for,flash)
from flaskext.login import LoginManager,current_user
from flask.ext.login import (LoginManager, current_user, login_required,login_user, logout_user, UserMixin,AnonymousUser, confirm_login, fresh_login_required)
from models.user import UserLog as User
app = Flask(__name__)

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active


class Anonymous(AnonymousUser):
    name = u"Anonymous"


USERS = {
    1: User(u"Admin", 1),
    2: User(u"Steve", 2),
    3: User(u"Creeper", 3, False),
}

USER_NAMES = dict((u.name, u) for u in USERS.itervalues())



SECRET_KEY = "yeah, not actually a secret"
DEBUG = True

app.config.from_object(__name__)

login_manager = LoginManager()

login_manager.anonymous_user = Anonymous
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))


login_manager.setup_app(app)

@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    message = None
    if request.method == "POST" and ("username" in request.form and "pwd" in request.form):
        username = request.form["username"]
        password = request.form["pwd"]
        if username in USER_NAMES:
            if password == "admin123":
                remember = request.form.get("remember", "no") == "yes"
                if login_user(USER_NAMES[username], remember=remember):
                    message = "Logged in!"
                    flash(message)
                    return redirect(request.args.get("next") or url_for("employee.index"))
                else:
                    message = "Sorry, but you could not log in."
                    flash(message)
            else:
                message= "Invalid password."
                flash(message)
        else:
            message= "Invalid username."
            flash(message)
    return render_template("login.html")


@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("employee.index"))


# Register blueprints here -----------------------------------------------------
from employee.views import employee


app.register_blueprint(employee, url_prefix='/employee')










