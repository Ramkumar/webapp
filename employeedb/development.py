#!/usr/bin/env python
from flaskext.script import Manager, Server
from app import app

from flask_principal import (Principal)

app.config.from_pyfile('development.cfg')

principals = Principal()
principals.__init__(app)
manager = Manager(app)

if __name__ == "__main__":
    
    manager.run()
