"""
Forms for adding and editing Employee Details
"""
from wtforms import (Form, BooleanField, TextField, TextAreaField,
        validators, IntegerField, SelectField, BooleanField, FileField, ValidationError,DateField,DateTimeField,PasswordField,FormField)


class AddressForm(Form):
    name = TextField(
        "Name", 
        [validators.Required('Name cannot be empty')]
        ) 

    Street = TextField(
        "Street", 
        [validators.Required('street cannot be empty')]
        )
    city = TextField(
        "city", 
        [validators.Required('city  cannot be empty')]
        )
    state = TextField(
        "state", 
        [validators.Required('state Name cannot be empty')]
        )
    pincode = TextField(
        "Pin Code",
        [validators.Required('Mobile No. should be number or can not be empty'),validators.Regexp("\d{6}",  message=u'Invalid input.')]
        )





class EmployeeForm(Form):
    empid = TextField(
        "Employee Id", 
        [validators.Required('Employee Id cannot be empty')]
        )
    
    empname = TextField(
        "Employee Name", 
        [validators.Required('Employee Name cannot be empty')]
        )
    
    fathername = TextField(
        "Father Name",
        [validators.Required('Father name  cannot be empty')]
        )
    mothername = TextField(
        "Mother Name",
        [validators.Required('Mother  name  cannot be empty')]
        )
    
    qualification = TextField(
        "Qualification Details",
        [validators.Required('Qualification Details cannot be empty')]
        )
    
    nationality = TextField(
        "Nationality",
        [validators.Required('Nationality cannot be empty')]
        )
    '''
    dob = DateField('Date:', validators=[DateRange(date(1919,1,1), date(2012,4,20))])
    '''
    dob= DateTimeField("Date of Birth",format='%m-%d-%Y')
   
    doj = DateTimeField("Date of Join",format='%m-%d-%Y')
        
    gender = SelectField(
         "Gender", 
         choices = [('male','MALE'),('female','FEMALE')])

    mstatus=SelectField(
        "Marital Status",
        choices = [('Single','SINGLE'),('Married','MARRIED')]
        )
    
    sname=TextField(
        "Spouse Name"
                )

    designation = TextField(
        "Designation",
        [validators.Required('Designation cannot be empty')]
        )

    department = TextField(
        "Department",
        [validators.Required('Department cannot be empty')]
        )

    email = TextField(
        "Email ID (for Payslip)",
        [validators.Required('Email ID  cannot be empty'),validators.Email(message="email should be in proper formate")]
        )

    pan = TextField(
        "PAN No.",
        [validators.Required('PAN card No. cannot be empty')]
        )

    mobile = TextField(
        "Mobile No.",
        [validators.Required('Mobile No. should be number or can not be empty'),validators.Regexp("\d{2}",  message=u'Invalid input.')]
        )
    address = FormField(AddressForm)

class UserForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    password = PasswordField('Password', [validators.Required('password can not be empty')
    ])

    
    
    

