from flask import Flask, request, render_template, redirect, url_for,Blueprint,flash,session
from forms import EmployeeForm,UserForm
from models.employeedb import Employee
from models.dbconfig import db
from models.user import UserLog as User
from app import app
from flask.ext.login import (LoginManager, current_user, login_required,login_user, logout_user,confirm_login, fresh_login_required)



employee = Blueprint('employee', __name__, template_folder='templates', static_folder='static')


@employee.route('/',methods=["GET", "POST"])
def index():
    return render_template('index.html')  



@employee.route('/list',methods=["GET", "POST"])
def show_all():
    emps = db.Employee.find()
    return render_template('employee_list.html', emps=emps)  

@employee.route('/views/<employee>',methods=["GET", "POST"])
def show_deatails(employee):
    emp=db.Employee()
    emp=emp.get_by_id(employee)
    return render_template('employee_views.html', emps=emp)                        


@employee.route('/new', methods=["GET", "POST"])
def add_employee():
    f =  EmployeeForm(request.form)
    if request.method == 'POST' and f.validate():
        emp = db.Employee()
        emp._id = f.empid.data
        emp.empname = f.empname.data
        emp.fathername = f.fathername.data
        emp.mothername = f.mothername.data
        emp.qualification = f.qualification.data
        emp.nationality = f.nationality.data
        emp.dob = f.dob.data
        emp.doj = f.doj.data
        emp.gender = f.gender.data
        emp.mstatus = f.mstatus.data
        emp.sname = f.sname.data
        emp.designation = f.designation.data
        emp.department = f.department.data
        emp.email = f.email.data
        emp.pan = f.pan.data
        emp.mobile = f.mobile.data
        emp.save()
        return redirect(url_for('.show_all'))
    return render_template('new_employee.html', form=f)


@employee.route('/edit/<employee>', methods=["GET", "POST"])
@login_required
def edit_employee(employee):
    emp=db.Employee()
    emp=emp.get_by_id(employee)
    form=EmployeeForm(request.form,obj=emp)
    form.empid.data=employee
    if request.method == 'POST' and form.validate():
        e=db.Employee()
        e.update(form,employee)
        return redirect(url_for('.show_all'))  
    return render_template('edit_employee.html',form=form,emp=emp)

@employee.route('/delete/<employee>', methods=["GET", "POST"])
@login_required
def delete_employee(employee):
    emp=db.Employee()
    emp.delete(employee)
    return redirect(url_for(".show_all"))




   


